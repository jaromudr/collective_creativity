module SettingReflection
  def settings_attr_accessor(*names)
    names.each do |name|
      define_method("#{name}") do
        self.settings.send(:"#{name}") || Setting.send(:"#{name}")
      end
      define_method("#{name}=") do |val|
        self.settings.send(:"#{name}=", val)
      end
    end
  end
end
