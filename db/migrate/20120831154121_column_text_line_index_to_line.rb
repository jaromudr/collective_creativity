class ColumnTextLineIndexToLine < ActiveRecord::Migration
   def change
    rename_column :variants, :text_line_id, :line_id
  end
end
