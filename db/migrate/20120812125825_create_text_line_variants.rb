class CreateTextLineVariants < ActiveRecord::Migration
  def change
    create_table :text_line_variants do |t|
      t.references :text_line
      t.string :text
      t.references :user

      t.timestamps
    end
    add_index :text_line_variants, :text_line_id
    add_index :text_line_variants, :user_id
  end
end
