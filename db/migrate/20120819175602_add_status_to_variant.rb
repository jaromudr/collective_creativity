class AddStatusToVariant < ActiveRecord::Migration
  def change
    add_column :variants, :status, :string
  end
end
