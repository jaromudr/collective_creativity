class CreateReadyVerses < ActiveRecord::Migration
  def change
    create_table :ready_verses do |t|
      t.string :title
      t.text :description
      t.references :user
      t.references :verse
      t.text :text

      t.timestamps
    end
    add_index :ready_verses, :user_id
    add_index :ready_verses, :verse_id
  end
end
