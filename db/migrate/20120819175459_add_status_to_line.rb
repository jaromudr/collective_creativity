class AddStatusToLine < ActiveRecord::Migration
  def change
    add_column :lines, :status, :string
  end
end
