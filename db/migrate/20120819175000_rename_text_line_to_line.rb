class RenameTextLineToLine < ActiveRecord::Migration
  def change
    rename_table :text_lines, :lines
    rename_table :text_line_variants, :variants
  end
end
