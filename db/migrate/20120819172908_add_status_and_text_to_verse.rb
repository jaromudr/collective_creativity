class AddStatusAndTextToVerse < ActiveRecord::Migration
  def change
    add_column :verses, :status, :string
    add_column :verses, :text, :text
  end
end
