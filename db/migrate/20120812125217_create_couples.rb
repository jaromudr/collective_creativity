class CreateCouples < ActiveRecord::Migration
  def change
    create_table :couples do |t|
      t.decimal :lines_number
      t.references :verse

      t.timestamps
    end
    add_index :couples, :verse_id
  end
end
