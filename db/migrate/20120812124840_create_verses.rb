class CreateVerses < ActiveRecord::Migration
  def change
    create_table :verses do |t|
      t.string :name
      t.references :user
      t.text :description

      t.timestamps
    end
    add_index :verses, :user_id
  end
end
