class CreateTextLines < ActiveRecord::Migration
  def change
    create_table :text_lines do |t|
      t.references :couple
      t.decimal :variants_number
      t.decimal :line_position

      t.timestamps
    end
    add_index :text_lines, :couple_id
  end
end
