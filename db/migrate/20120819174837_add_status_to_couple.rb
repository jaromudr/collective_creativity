class AddStatusToCouple < ActiveRecord::Migration
  def change
    add_column :couples, :status, :string
  end
end
