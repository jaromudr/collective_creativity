class VersesController < ApplicationController
  def index
    if params[:username]
      @verses = User.find(:username=>params[:username]).verses
    else
      @verses = Verse.all      
    end      
    
    respond_to do |format|
      format.html
      format.json { render :json => @verses }
    end
  end
  
  def show
    @verse = Verse.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render :json => @verse }
    end
  end
  def new
    @verse = Verse.new
    respond_to do |format|
      format.html
      format.json { render :json => @verse}
    end
  end
  def create
    user = User.find(:first, :conditions => ["username = ?", "jaromudr"])
    @verse = user.verses.create(params[:verse])
    respond_to do |format|
      if @verse
        format.html { redirect_to(@verse) } 
        format.json { render :json => @verse, :status => "created", :location => @verse }
      else
        format.html { render :action => "new" }
        format.json {render :json => @verse.error, 
                            :status => :unprocessable_entity}
      end
    end    
  end  
  def edit
    @verse = Verse.find(params[:id])
  end
  def update
    @verse = Verse.find(params[:id])
    respond_to do |format|
      if @verse.update_attributes(params[:verse])
        format.html  { redirect_to(@verse) }
        format.json  { head :no_content }
      else
        format.html  { render :action => "edit" }
        format.json  { render :json => @post.errors,
                              :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @verse = Verse.find(params[:id])
    @verse.destroy
    respond_to do |format|
      format.html { redirect_to verses_url }
      format.json { head :no_content }
    end
  end      
end
