class User < ActiveRecord::Base
  attr_accessible :password, :username
  validates_uniqueness_of :username
  
  has_many :verses
  has_many :ready_verses
  has_many :variants  
  ajaxful_rater
  
  def self.authenticate(username, password)
    user = User.find(:first, :conditions=>["username = ?", username])
    return nil if user.nil?
    return user if user.password==user.password
  end
  
  def verses_in_progress
    Verse.where(:status => "progress")
  end  
  
  def verses_involving    
    Variant.where("user_id"=>self.id).map(&:verse).uniq      
  end
end
