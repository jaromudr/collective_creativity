class Variant < ActiveRecord::Base
  belongs_to :line
  belongs_to :user
  has_one :couple, :through => :line
  has_one :verse, :through => :couple
  attr_accessible :text
  
  ajaxful_rateable :stars => 10
end
