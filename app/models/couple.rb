class Couple < ActiveRecord::Base
  include RailsSettings::Extend
  
  belongs_to :verse  
  attr_accessible :lines_number, :status
  
  has_many :lines
  has_many :variants, :through => :lines  
end
