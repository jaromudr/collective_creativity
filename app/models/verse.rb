class Verse < ActiveRecord::Base
  include RailsSettings::Extend  
  extend SettingReflection
  
  belongs_to :user  
  attr_accessible :description, :name, :status, :text
  
  attr_protected :id
  
  has_many :couples
  has_many :lines, :through => :couples
  has_many :variants, :through => :lines
  has_one :ready_verse  

  settings_attr_accessor :couple_num, :line_num
end
