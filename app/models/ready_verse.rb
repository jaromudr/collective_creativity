class ReadyVerse < ActiveRecord::Base
  belongs_to :user, :verse      
  attr_accessible :description, :text, :title
  
  ajaxful_rateable :stars => 10
  
end
