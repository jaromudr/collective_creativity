class Line < ActiveRecord::Base
  include RailsSettings::Extend
  
  belongs_to :couple
  has_one :verse, :through => :couple
  attr_accessible :variants_number, :line_position
  
  has_many :variants  
end
